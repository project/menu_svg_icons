<?php

namespace Drupal\Tests\menu_svg_icons\Functional;

use Drupal\menu_svg_icons\Entity\IconSet;
use Drupal\Tests\BrowserTestBase;

/**
 * Test menu svg admin UI.
 *
 * @group menu_svg_icons
 */
class MenuSvgIconsUiTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = ['menu_svg_icons', 'block'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Make sure local actions are available as links.
    $this->drupalPlaceBlock('local_actions_block');
  }

  /**
   * Test icon set admin actions namely; creation and deletion.
   */
  public function testIconSetCreation() {
    // Create a new user which has access to the menu svg icon settings.
    $this->drupalLogin($this->createUser(['administer site configuration']));
    $this->drupalGet('admin/config/media/menu-svg-icons/icon-set');

    // Test to creating a new icon set.
    $this->assertSession()->linkByHrefExists('/admin/config/media/menu-svg-icons/icon-set/add');
    $this->clickLink('Add icon set');
    $this->submitForm([
      'label' => 'Test',
      'description' => '',
      'placement' => 'left',
      'source' => '<svg xmlns="http://www.w3.org/2000/svg" version="1.1"><defs><symbol viewBox="0 0 8 8" id="arrow-bottom"><path transform="translate(1)" d="M2 0v5h-2l2.531 3 2.469-3h-2v-5h-1z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-circle-bottom"><path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm-1 1h2v3h2l-3 3-3-3h2v-3z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-circle-left"><path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1v2h3v2h-3v2l-3-3 3-3z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-circle-right"><path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1l3 3-3 3v-2h-3v-2h3v-2z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-circle-top"><path d="M4 0c-2.21 0-4 1.79-4 4s1.79 4 4 4 4-1.79 4-4-1.79-4-4-4zm0 1l3 3h-2v3h-2v-3h-2l3-3z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-left"><path transform="translate(0 1)" d="M3 0l-3 2.531 3 2.469v-2h5v-1h-5v-2z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-right"><path transform="translate(0 1)" d="M5 0v2h-5v1h5v2l3-2.531-3-2.469z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-thick-bottom"><path transform="translate(1)" d="M2 0v5h-2l3.031 3 2.969-3h-2v-5h-2z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-thick-left"><path transform="translate(0 1)" d="M3 0l-3 3.031 3 2.969v-2h5v-2h-5v-2z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-thick-right"><path transform="translate(0 1)" d="M5 0v2h-5v2h5v2l3-3.031-3-2.969z"></path></symbol><symbol viewBox="0 0 8 8" id="arrow-thick-top"><path transform="translate(1)" d="M2.969 0l-2.969 3h2v5h2v-5h2l-3.031-3z"></path></symbol></defs></svg>',
      'icon_height' => '',
      'icon_width' => '',
    ], 'Save');

    $this->assertSession()->pageTextContains('Saved the Test Icon set.');
    $this->assertSession()->addressEquals('admin/config/media/menu-svg-icons/icon-set');

    // Test deleting the newly created icon set.
    $this->assertSession()->linkByHrefExists('/admin/config/media/menu-svg-icons/icon-set/test/delete');
    // Index 1 as the module comes with a default 'Arrow' icon set.
    $this->clickLink('Delete', 1);
    $this->assertSession()->pageTextContains('Are you sure you want to delete the Test icon set?');
    $this->clickLink('Delete');
    $this->assertSession()->pageTextContains('Test icon set has been deleted.');

    // Make sure the entity was actually deleted.
    $this->assertNull(IconSet::load('test'), 'Icon Set entity "Test" does not exist');
  }

}
